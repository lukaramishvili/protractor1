
export class Number {
    
}
export class String {
    
}
export class Date {
    
}

export class Client {    
    id: Number;
    registerDate: Date;
    firstName: String;
    lastName: String;
    
}

export class Order {
    order_id: Number;//შეკვეთის კოდი :არის რიცხვი
    order_date: Date;//შეკვეთის თარიღი :არის თარიღი (წარმოადგენს თარიღს)
    amount: Number;
    gayiduli_produqtebi: SoldItem[];// გაყიდული პროდუქტები :არის SoldItem ტიპის.
    jamuri_tanxis_datvla: function(){//ჯამური_თანხის_დათვლა :არის ფუნქცია (ანუ იგივე მოქმედება)
        let jami = 0;
        //ვთქვათ ჯამი არის ნოლი;
        foreach(let rac_xelshi_gichiravs in this.gayiduli_produqtebi){
            jami += rac_xelshi_gichiravs.price * rac_xelshi_gichiravs.count;
            //ჯამს მიუმატე რაც ხელში გიჭირავს იმისი ფასი გამრავლებული რაც ხელში გიჭირავს იმის რაოდენობაზე;
        }
        // add vat
        jami = jami * 1.18;//ჯამი =გახადე ჯამი გამრავლებული ერთი მთელი თვრამეტ მეასედზე (დაუმატე დღგ)
        return jami;
        //დაუბრუნე პასუხი ჯამი რაც ფურცელზე გიწერია;
    }
}


export class Product {
    id: Number;
    name: String;
    price: Number;
    darchenilia: Number;
}

export class SoldItem {
    id: Number;
    product: Product;
    count: Number;
    order: Order;
}

let registraciis_tarigi = new Date('2019-08-07');
let user_id: Number = 5;

let gios_eqaunti = new Client({ id: 5, registerDate: '2019-08-07', first_name: "gio" });

let givikos_order = new Order({ 
    order_id: 355, order_date: '2019-09-18 13:45:01', amount: 3500,
    gayiduli_produqtebi: [  new SoldItem({ product_id: 1, count: 3 }) ,  new SoldItem({ product_id: 2, count: 1 })  ];
});
let geles_order = new Order({ order_id: 356, order_date: '2019-09-18 13:55:01', amount: 200 });

let geles_iphone = new SoldItem({ id: 3456356, count: 1, order: geles_iphone })

print geles_iphone.order.amount