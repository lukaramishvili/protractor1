

// spec.js
describe('Protractor Demo App', function() {
  it('should have a title', function() {
    browser.waitForAngularEnabled(false);
    
    browser.get('https://vendoo.ge');
    
    expect(browser.getTitle()).toEqual("Vendoo.ge | ვენდუ ონლაინ სავაჭრო ცენტრი");
  });
});